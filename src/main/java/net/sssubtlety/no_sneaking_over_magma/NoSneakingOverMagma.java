package net.sssubtlety.no_sneaking_over_magma;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minecraft.text.Text;

public class NoSneakingOverMagma {
    public static final String NAMESPACE = "no_sneaking_over_magma";
    public static final Text NAME = Text.translatable("text." + NAMESPACE + ".name");

    public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);
}
