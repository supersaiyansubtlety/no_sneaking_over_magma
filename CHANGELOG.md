- 1.2.1 (15 Dec. 2024): Marked as compatible with 1.21.4
- 1.2.0 (30 Nov. 2024): Updated for 1.21.2-1.21.3
- 1.1.1 (4 Sep. 2024): Marked as compatible with 1.21.1
- 1.1.0 (17 Jul. 2024):
  - Updated for 1.21!
  - Replaced bundled [CrowdinTranslate](<https://github.com/gbl/CrowdinTranslate>) with optional
    [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
    install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates
  - Moderate internal changes
- 1.0.22 (9 May 2024):
  - Marked as compatible with 1.20.6
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
- 1.0.21 (23 Apr. 2024): Marked as compatible with 1.20.5
- 1.0.20 (29 Jan. 2024):
  - Marked as compatible with 1.20.3 and 1.20.4
  - Minor internal changes
- 1.0.19 (23 Oct. 2023): Updated for 1.20.2
- 1.0.18 (14 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.0.17 (20 Mar. 2023): Updated for 1.19.4
- 1.0.16 (7 Aug. 2022): Marked as compatible with 1.19.2
- 1.0.15 (29 Jul. 2022): Marked as compatible with 1.19.1
- 1.0.14 (28 Jun. 2022): Updated for 1.19!
- 1.0.13 (15 Mar. 2022):
  
  - reduced minimum required Fabric API version
  - bundled new 1.18.2 Crowdin Translate so translations can be updated when the game launches

- 1.0.12 (4 Mar. 2022):
  
  - Updated for 1.18.2
  - Removed Crowdin Translate until it updates for 1.18.2
  - Cloth config is now optional (and autocofig needn't be installed separately)
  - Changed config names, so you'll need to re-set any configs you changed

- 1.0.11 (12 Dec. 2021): Marked as compatible with 1.18.1
- 1.0.10 (8 Dec. 2021): Updated for 1.18!
- 1.0.9 (8 Dec. 2021): 

  Configs no longer require a restart to take effect!  

  Added new options!
  
  - Magma damages non-living entities (defaults to false)
    
  - Magma sets fire to entities (defaults to false)
  
  Reworked mixins, improving theoretical compatibility.

- 1.0.8 (9 Jul. 2021): Marked as compatible with 1.17.1.
- 1.0.7 (13 Jun. 2021): 
  - Updated for 1.17
  - No longer bundles Cloth Config and Auto Config (updated), these must be installed separately
- 1.0.6 (15 Jan. 2021): Translations are now handled through [CrowdinTranslate](https://crowdin.com/project/no-sneaking-over-magma).
  Marked as compatible with 1.16.5. 
- 1.0.5 (3 Nov. 2020): Marked as compatible with 1.16.4. 
- 1.0.4 (18 Sep. 2020): Marked as compatible with 1.16.3. 